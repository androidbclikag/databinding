package com.example.databinding

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.databinding.databinding.BandModelLayoutBinding

class RecyclerViewAdapter(private val bands: ArrayList<ItemModel>) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    inner class ViewHolder( private val binding: BandModelLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.bandModel = bands[adapterPosition]
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.band_model_layout,
            parent,
            false
        )
    )

    override fun getItemCount(): Int = bands.size

    override fun onBindViewHolder(holder: RecyclerViewAdapter.ViewHolder, position: Int) = holder.onBind()

}