package com.example.databinding

interface CustomCallBack {
    fun onFailure(response:String){}
    fun onResponse(response:String) {}
}