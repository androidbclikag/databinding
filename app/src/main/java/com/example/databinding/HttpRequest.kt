package com.example.databinding

import android.util.Log
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

object HttpRequest {
    const val BANDS = "5ec3ab0f300000850039c29e"
    private var retrofit = Retrofit.Builder().addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("http://www.mocky.io/v2/")
        .build()
    private var service = retrofit.create(
        ApiRequest::class.java)


    private fun onCallBack(callback: CustomCallBack) = object : Callback<String> {
        override fun onFailure(call: retrofit2.Call<String>, t: Throwable) {
            Log.d("fail", "${t.message}")
            callback.onFailure(t.message.toString())
        }

        override fun onResponse(call: retrofit2.Call<String>, response: Response<String>) {
            Log.d("fail", "${response.body()}")
            callback.onResponse(response .body().toString())
        }


    }




    fun getRequest(path: String, callback: CustomCallBack) {
        val request = service.getRequest(path)
        request.enqueue(
            onCallBack(
                callback
            )
        )

    }

    interface ApiRequest {
        @GET("{path}")
        fun getRequest(@Path("path") path: String): retrofit2.Call<String>
    }
}