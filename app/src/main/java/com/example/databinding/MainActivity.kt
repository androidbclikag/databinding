package com.example.databinding

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity() {
    private val items = ArrayList<ItemModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        recyclerView.layoutManager = GridLayoutManager(this, 2)
        recyclerView.adapter = RecyclerViewAdapter(items)
        getRequest()
    }


    private fun parseJson(body: String) {
        val json = JSONArray(body)
        for (index in 0 until json.length()) {
            val data = json[index] as JSONObject
            val itemModel = ItemModel()
            itemModel.imageUrl = data.getString("img_url")
            itemModel.name = data.getString("name")

            items.add(itemModel)
            recyclerView.adapter!!.notifyDataSetChanged()
        }
    }

    private fun getRequest() = HttpRequest.getRequest(HttpRequest.BANDS,
        object : CustomCallBack {
            override fun onFailure(response: String) {

            }

            override fun onResponse(response: String) {
                parseJson(response)
            }
        })
}
