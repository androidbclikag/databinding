package com.example.databinding

import android.widget.ImageView

import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

object DataBindComponents {
    @JvmStatic
    @BindingAdapter("setImage")
    fun setImage(imageView: ImageView, imageUrl: String?) {
        if (!imageUrl.isNullOrEmpty()) {
            Glide
                .with(imageView.context)
                .load(imageUrl)
                .placeholder(R.mipmap.ic_launcher)
                .into(imageView)

        }

    }
}